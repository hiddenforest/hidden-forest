A BEAUTIFUL RENTAL COMMUNITY IN FAIRLESS HILLS, PENNSYLVANIA
Newly renovated units available now , call for specials! (215) 946-1999
Hidden Forest Apartment Features
Newly upgraded kitchens with stainless steel appliances
Individually controlled heating and air conditioning
New luxury flooring available in many units
Washer and dryer in every unit.

Address: 602 Hidden Forest Ct, Fairless Hills, PA 19030, USA
Phone: 215-946-1999
